import { Side } from '../../typings'
import { Kraken } from '../'

const kraken = new Kraken({
  api_key: '',
  api_secret: '',
  symbol: 'btcusd'
})

kraken.cancelAll()

kraken.placeMarketOrder({
  amount: 1.2,
  side: Side['BUY']
})
