import { Exchange, ClientOpts, MarketOrderOpts, Orderbook } from '../../typings/index'

export default class Kraken implements Exchange {
  public market: string
  constructor(clientOpts: ClientOpts) {
    this.market = 'kraken'
  }
  getOrderBook(): Promise<Orderbook> {
    return Promise.resolve()
      .then((): Orderbook => {
        return {
          market: this.market,
          asks: [
            {
              amount: 1.2,
              price: 2.1
            }
          ],
          bids: [
            {
              amount: 1.2,
              price: 2.0
            }
          ]
        }
      })
  }
  placeMarketOrder(opts: MarketOrderOpts): Promise<any> {
    return Promise.resolve('place market order')
  }
  cancelOrder(id: string): Promise<any> {
    return Promise.resolve(1)
  }
  cancelAll(): void {
    console.log('cancel all')
  }
  checkBalance(): void {}
}
