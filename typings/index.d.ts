export interface ClientOpts {
  api_key: string
  api_secret: string
  symbol: string
}

export enum Side {
  BUY = 'buy',
  SELL = 'sell'
}

export interface MarketOrderOpts {
  amount: Number
  side: Side
}

export interface OrderRecord {
  amount: Number
  price: Number
}

export interface Orderbook {
  market: string
  asks: Array<OrderRecord>
  bids: Array<OrderRecord>
}

export interface Exchange {
  getOrderBook(): Promise<Orderbook>
  placeMarketOrder(opts: MarketOrderOpts): Promise<any>
  cancelOrder(id: string): Promise<any>
  cancelAll(): void
  checkBalance(): void
}

declare class Bitfinex implements Exchange {
  public makret: string
  constructor(opts: ClientOpts)
  getOrderBook(): Promise<Orderbook>
  placeMarketOrder(opts: MarketOrderOpts): Promise<any>
  cancelOrder(id: string): Promise<any>
  cancelAll(): void
  checkBalance(): void
}

declare class Kraken implements Exchange {
  public makret: string
  constructor(opts: ClientOpts)
  getOrderBook(): Promise<Orderbook>
  placeMarketOrder(opts: MarketOrderOpts): Promise<any>
  cancelOrder(id: string): Promise<any>
  cancelAll(): void
  checkBalance(): void
}
